#!/usr/bin/env coffee
# WPsuck
#> CLI to R/W WP posts to/from local HTML file.

### Synopsis
```$ wpsuck --down=post.html --post_id=961 --website=https://decodecode.net/elitist --user=$U --password=$P

$ wpsuck --up=post.html

$ wpsuck --help
```

For more about this hack see my devlog: https://decodecode.net/elitist/2019/07/wpsuck/ .
###

## Dependencies
assert=require 'assert'
fse=require 'fs-extra' # Promisified fs.
require 'colors'

## Parse CLI
argv=(require 'minimist') process.argv.slice 2

# Configure obvious request options.
request=(require 'request').defaults gzip:yes # Sure, optimize.

## Polyfills (because might run this with ancient Node).
String::endsWith?=(suffix)->suffix is @substring @length-suffix.length
String::startsWith?=(prefix)->prefix is @substring 0,prefix.length
unless Promise then Promise=require 'promise' # (Can't use ?= on variables, only attributes.)

## Utils
strip_trailing=(t,s)->if s.endsWith t then s.slice 0,-t.length else s

## API
wpsuck_api=

## `down`
# @returns (promise of) post's HTML content as string.
#? Should return entire post's JSON, so API users can work with, eg, title, not just content!
	down:(website,auth,post_id)->
		return new Promise (resolve,reject)->
			assert website and auth and post_id,"ERROR: invalid options: #{JSON.stringify argv}.".red
			url=(strip_trailing '/',website)+'/wp-json/wp/v2/posts/'+post_id
			options={url,auth}
			unless argv.quiet then console.error "request.GET #{url} …"
			request.get options,(error,response,body)->
				body=JSON.parse body #? Why instead json:true?
				if error
				then reject error
				else unless body.content?.rendered
				then reject body
				else resolve body.content.rendered

## `up`
	up:(website,auth,post_id,data)->
		return new Promise (resolve,reject)->
			assert website and auth and post_id and data,"ERROR: invalid parameters: #{JSON.stringify {website,auth,post_id,data}}.".red
			url=(strip_trailing '/',website)+'/wp-json/wp/v2/posts/'+post_id
			options={url,auth,body:data,json:yes}
			unless argv.quiet then console.error "request.POST #{url} …"
			request.post options,(error,response,body)->
				#?console.log response
				#?body=JSON.parse body
				if error
				then reject error
				else unless response.statusCode is 200
				then reject body
				else resolve body

## Action!
if argv.website and not argv['force-unsafe'] then assert argv.website.startsWith 'https://',"ERROR: must use HTTPS! Because credentials passed in clear text. \"#{website}\". (Workaround with --force-unsafe.)".red

switch
	when argv.help
		#? Seems I won't be doing I/O with stdin/stdout, so messages to stderr seem pointless?
		console.error '''--down TO_FILE: download post content and save TO_FILE, or stdout.
		--up FROM_FILE: upload content from FROM_FILE
		--website URL: must start with "https"
		--user USER
		--password PASS
		--post_id ID
		--force-unsafe: don\'t insist on HTTPS
		--quiet: less verbose
		--help: display this'''

	when argv.down
		{website,user,password,post_id}=argv
		assert website and user and password and post_id,"ERROR: invalid options: #{JSON.stringify argv}.".red
		unless argv.quiet then console.error 'Downloading…'
		auth={user,password} # HTTP Basic.
		wpsuck_api
		.down website,auth,post_id
		.then ((content)->
			unless argv.quiet then console.error "Content received: #{content.length} bytes."
			# Save configuration to output: prepend to content as HTML comment.
			config="<!--#{JSON.stringify {website,user,password,post_id}}-->\n"
			fse.writeFile argv.down,config+content
			.then (->unless argv.quiet then console.error ';o)'.green.inverse
			),(reason)->console.error 'ERROR: saving to file failed!\n'.red,reason
		),(reason)->console.error 'ERROR: downloading failed!\n'.red,reason

	when argv.up
		unless argv.quiet then console.log 'Uploading…'
		# Extract configuration from file:
		fse.readFile argv.up,'utf8'
		.then ((f)->
			unless argv.quiet then console.error f.length,'bytes read.'
			m=/<!--(.+)-->\n([^]+)/.exec f
			{website,user,password,post_id}=JSON.parse m[1]
			assert website and user and password and post_id,"ERROR: invalid configuration: \"#{m[1]}\".".red

			#? … Override from CLI, or environment?
			#?{user,password}=argv
			auth={user,password} # HTTP Basic.
			# Filter content.
			count=empty:0,hacks:0 #? Ugly?
			censored=(s)->switch
				when s is '' # Remove empty lines.
					count.empty++
					false
				when /^<(meta|script|link)[^>]*>(.*<\/script>)?$/i.test s # Remove hacks for local dev.
					count.hacks++
					false
				else true
			content=m[2]
			.split '\n'
			.filter (s)->censored s
			.join '\n'
			if count.empty or count.hacks then console.error "Censored lines:".red,count
			# Payload must follow schema spec…
			payload={content} #?payload=JSON.stringify content:m[2]
			wpsuck_api
			.up website,auth,post_id,payload
			.then ((body)->unless argv.quiet then console.error ';o)'.green.inverse #? console.log body
			),(reason)->console.error 'ERROR: uploading failed!\n'.red,reason
		),(reason)->console.error 'ERROR: can\'t read file!\n'.red,reason

	else
		console.error 'ERROR: missing command! Must specify either --up or --down. See --help.'.red
