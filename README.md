# WPsuck
> CLI to R/W WP posts to/from local HTML file.

A hack around the WordPress editor's (raw/HTML mode) problems. [Explanation](https://decodecode.net/elitist/2019/07/wpsuck/) posted to my devlog.

## Synopsis

	$ wpsuck --down=post.html --post_id=961 --website=https://decodecode.net/elitist --user=$U --password=$P

	$ wpsuck --up=post.html

	$ wpsuck --help

## Installation

1. Download the code
2. Assuming ~/bin/ is your $PATH:

	$ ln --symbolic $PWD/wpsuck.coffee ~/bin/wpsuck

## Development

https://gitlab.com/decodecode/wpsuck

Feedback welcome!

## Release notes

- Filter uploaded content: remove hacks, empty lines.
- Colorful console.

### 1.0.0, released 2019-07-02

- Seems useful and stable enough. ;o)
